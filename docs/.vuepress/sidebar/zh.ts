import {sidebar} from "vuepress-theme-hope";


export const zhSidebarConfig = sidebar(
    {
        "/deploy/": "structure",
        "/usage/": "structure",
        "/store/": "structure",
        "/dev/": "structure",
    }
)